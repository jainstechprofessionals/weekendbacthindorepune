
public class RecursionInJava {
	int a = 2;

	public void show() {
		System.out.println(a);
		a = a + 2;
		if (a <= 20) {
			
			show();
		}

	}

	
	public int factorial(int n) {
		if(n==1) {
			return 1;
		}
		else {
			return (n*factorial(n-1));
		}
		
	}
	
	
	public static void main(String[] args) {
		RecursionInJava obj = new RecursionInJava();
		obj.show();
		
		int a =obj.factorial(5);
		System.out.println(a);

	}

}

 // factorial(5)

   // factorial(4)
   
    // factorial(3)
   
    // factorial(2)
   
     // (factorial(1)
        
//        retrun 1;
//        retrun 2*1
//        return 3*2
//        return 4*6
//        retrun 5*24
//        
       














