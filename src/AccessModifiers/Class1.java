package AccessModifiers;

import java.util.Scanner;

public class Class1 {

	
	public void show() {
		display();
		
	}
	
	private int display() {
		return 9;
	}
	
	
	protected void view() {
		
	}
	
	
	void see() {
		
	}
	
	public static void main(String[] args) {
		Class1 obj = new Class1();
		
		Scanner sc = new Scanner(System.in);
		System.out.println("please enter a value");
		int s = sc.nextInt();
		
		System.out.println(s);
		
	obj.display();
	obj.view();
	obj.show();
	obj.see();

	}

}
