
public class ThisR {
	
	int t=10;
	
	ThisR(){
		System.out.println("in thisr");
	}
	
	
	ThisR(int i){
		this();
		System.out.println(i);
	}
	
	public void show(int t) {
	
		
		System.out.println(t);
		
		
		System.out.println(this.t);
		
	}
	
	public void eat() {
		this.show(20);
	}

	public static void main(String[] args) {
		
		
		ThisR o = new ThisR(90);
		
		
		o.show(20);

	}

}
