package JavaProgram;

public class PolicyBaazar {
	Bank obj;

	public void retrunRateOfInteres(String str) {
		if (str.equals("sbi")) {

			obj = new Sbi(); // upcasting, run time polymorphism, dynamic method dispatch , late binding
								// ,dynamic binding

		}

		if (str.equals("icici")) {

			obj = new Icici();

		}

		show();

	}
	
	
	public void show() {
		System.out.println(obj.rateOfInterest());
	}

	public static void main(String[] args) {

		// Sbi o = new Sbi(); //static binding ,early binding

		PolicyBaazar obj = new PolicyBaazar();

		obj.retrunRateOfInteres("sbi");

	}

}



//maven 
// git

