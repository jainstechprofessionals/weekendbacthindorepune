package Collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ArrayListJava {

	public static void main(String[] args) {
	
		List<String> ar = new ArrayList<String>();
		
	
		ar.add("rounak");
		ar.add("tarun");
		ar.add("apporva");
		ar.add("himanshu");
		ar.add("rounak");
		
		
		ar.add(2,"roli");
	
		ar.set(3, "vaishali");
		
//		for(int i=0;i<ar.size();i++) {
//			System.out.println(ar.get(i));
//		}
		
		
//		Iterator itr = ar.iterator();		
//		while(itr.hasNext()) {
//			System.out.println(itr.next());
//		}

		
		
		Collections.sort(ar);
		
		
		for(String str :ar) {
			System.out.println(str);
		}
		
		
		

	}

}
