package Collection;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class HashSetJava {

	public static void main(String[] args) {
		
		Set< String> ar = new LinkedHashSet<String>();
		
		ar.add("rounak");
		ar.add("tarun");
		ar.add("apporva");
		ar.add("himanshu");
		ar.add("rounak");
		
		ar.add(null);
		
		System.out.println(ar);
		
		Iterator str = ar.iterator();
		while(str.hasNext()){
			System.out.println(str.next());
		}
	}

}
