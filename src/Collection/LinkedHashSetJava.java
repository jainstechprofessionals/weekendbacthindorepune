package Collection;

import java.util.HashSet;
import java.util.Iterator;

public class LinkedHashSetJava {

	public static void main(String[] args) {
		
		HashSet< String> ar = new HashSet<String>();
		
		ar.add("rounak");
		ar.add("tarun");
		ar.add("apporva");
		ar.add("himanshu");
		ar.add("rounak");
		
		ar.add(null);
		
//		System.out.println(ar);
		
		Iterator str = ar.iterator();
		while(str.hasNext()){
			System.out.println(str.next());
		}
	}

}
