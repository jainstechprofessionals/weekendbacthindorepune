import java.util.Scanner;

public class SwitchStatement {

	public static void main(String[] args) {
		
		Scanner obj = new Scanner(System.in);
		System.out.println("please enter a number");
		int a = obj.nextInt();

		switch (a) {

		case 1:
			System.out.println("it is jan");
			break;
		case 2:
			System.out.println("it is feb");
			break;
		
		case 3: 
			System.out.println("it is march");
			break;
		default:
			System.out.println("please enter a number between 1 to 12");
		}

	}

}
