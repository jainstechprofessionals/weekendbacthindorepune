
public class InstanceAndLocalVariable {

	int a ; // instance variable
	double radius;
	
	
	public void set(int c,double d) {
		a=c;
		radius=d;
	}
	
	
	public void sum(int b) {
		System.out.println(a+b);
	}
	
	public void mul(int b) {
		System.out.println(a*b);
	}
	
	
	
	public void circleArea() {
		System.out.println(Math.PI *radius*radius);
	}
	
	
	public void circumtances() {
		System.out.println(Math.PI*2*radius);
	}
	
	public static void main(String[] args) {
		
		InstanceAndLocalVariable obj = new InstanceAndLocalVariable();
		obj.set(5, 10.2);
//		obj.sum(10);
//		obj.mul(40);
		obj.circleArea();
		obj.circumtances();
		
		
	}

}
